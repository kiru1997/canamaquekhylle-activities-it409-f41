@extends('layouts.app')

@section('content')
<div class="container-fluid m-t-40">

    <div class="card">
        <div class="card-body">
            <form action="{{ route('file.store') }}" method="POST" name="add_product" enctype="multipart/form-data">
                {{ csrf_field()  }}
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <strong>Title</strong>
                            <input type="text" name="title" class="form-control" placeholder="Enter Title">
                            <span class="text-danger">{{ $errors->first('title') }}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <strong>Product Code</strong>
                            <input type="text" name="product_code" class="form-control" placeholder="Enter Product Code">
                            <span class="text-danger">{{ $errors->first('product_code') }}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <strong>Description</strong>
                            <textarea class="form-control" col="4" name="description" placeholder="Enter Description"></textarea>
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <strong>Product Image</strong>
                            <input type="file" name="image" class="form-control" placeholder="">
                            <span class="text-danger">{{ $errors->first('image') }}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="table-responsive m-t-40">
                <table id="example23_wrapper" class="display nowrap table table-hover table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Title</th>
                            <th>Product Code</th>
                            <th>Description</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($files as $file)
                        <tr>
                            <td><img src="{{ asset('../../image') }}" style="width:80px;"></td>
                            <td>{{$file->title}}</td>
                            <td>{{$file->product_code}}</td>
                            <td>{{$file->description}}</td>
                            <td>
                                <div class="d-flex justify-content-center">
                                    <div class="m-l-5 m-r-5">
                                        <form method="POST" action="{{ route('file.destroy', $file->id) }}">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" onclick="return confirm('Are you sure you want to delete this data?')" class="btn btn-sm btn-danger" title="Delete" data-original-title="Delete"><i class="ti-trash"></i></button>
                                        </form>
                                    </div>
                                    <a href="" class="text-inverse m-l-5 m-r-5" title="" data-toggle="modal" data-target="#file{{$file->id}}" data-original-title="Delete"><i class="ti-pencil"></i></a>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @foreach($files as $file)
    <div id="file{{$file->id}}" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Edit File</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form method="POST" action="{{ route('file.update', $file->id) }}">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="">Title</label>
                                <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}{{$file->title}}" required autocomplete="title" autofocus placeholder="Title">
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                                <div class="m-t-10">
                                    <label for="">Product Code</label>
                                    <input type="text" class="form-control @error('product_code') is-invalid @enderror" name="product_code" value="{{ old('product_code') }}{{$file->product_code}}" required autocomplete="product_code" autofocus placeholder="Product Code">
                                    @error('product_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                                <div class="m-t-10">
                                    <label for="">Description</label>
                                    <input type="text" class="form-control @error('description') is-invalid @enderror" name="description" value="{{ old('description') }}{{$file->description}}" required autocomplete="description" autofocus placeholder="Description">
                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary waves-effect">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @endforeach
</div>
@endsection